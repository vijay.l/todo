import React from 'react'

export interface ITodoItem{
    id: number;
    text: string;
    created_at: string
}

export interface ITodoList{
    items: Array<ITodoItem>
}

const sampleJson:ITodoList = {
    "items":[
        {
    "id" : 1,
    "text": "Learn about Polymer",
    "created_at": "Mon Apr 26 06:01:55 +0000 2015",
        },
        {
    "id" : 2,
    "text" : "Watch Pluralsight course on Docker",
    "created_at" : "Tue Mar 02 07:01:55 +0000 2015",
        },
        {
    "id" : 3,
    "text" : "Complete presentation prep for Aurelia presentation",
    "created_at" : "Wed Mar 05 10:01:55 +0000 2015",
        },
        {
    "id" : 4,
    "text": "Instrument creation of development environment with Puppet",
    "created_at" : "Fri June 30 13:00:00 +0000 2015",
        },
        {
    "id" : 5,
    "text": "Transition code base to ES6",
    "created_at" : "Mon Aug 01 10:00:00 +0000 2015",
        }
      ]
    }

    export default sampleJson