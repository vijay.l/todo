import React, { SyntheticEvent } from 'react'
import {JsonToTable} from 'react-json-to-table'
import sampleJson, {ITodoList,ITodoItem} from '../SampleJson'


let item: ITodoItem = {id: 0,text: '', created_at: ''}
let text = {}
let retrievedJson: ITodoList = JSON.parse(String(localStorage.getItem("todoList")))
  ? JSON.parse(String(localStorage.getItem("todoList")))
  : sampleJson
let length: number = Object.keys(retrievedJson.items).length as number

console.log(length)

const handleChange: React.EventHandler<SyntheticEvent> = (event) => {
  text = event
  console.log("Jsonbef", retrievedJson)
}

const handleSubmit = () => {
  console.log("handlSub", retrievedJson)
  // let items = retrievedJson.items
  // console.log("itt", items)
  let date = new Date()
  item = {
    id: length + 1,
    text: String(text),
    created_at: date.toUTCString(),
  }
  console.log(item)
  retrievedJson.items.push(item)
  console.log("itt2", retrievedJson)
  localStorage.setItem("todoList", JSON.stringify(retrievedJson))
  console.log("Jsonaf", JSON.parse(String(localStorage.getItem("todoList"))))
}
const AddTodo: React.FC = () => {
    
    return <form>
        <input type="text" placeholder="Add Task"/>
        <button type="submit">Add</button>
    </form>
}

export const Todo: React.FC = () => {
    if (!JSON.parse(String(localStorage.getItem("todoList")))) {
        localStorage.setItem('todoList',String(sampleJson))
      }
      let retrievedJson = localStorage.getItem("todoList")
    return <div>
        <AddTodo />
        <JsonToTable json={JSON.parse(String(retrievedJson))} />
    </div>
}